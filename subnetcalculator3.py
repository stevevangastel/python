import sys

def welcome():
    print("*************************************************")
    print("********** Welcome to subnetcalculator **********")
    print(f"{'***':^10}",f"{' ':^27}",f"{'***':^10}")
    print(f"{'*':^10}",f"{' ':^27}",f"{'*':^10}")

def ending():
    print(f"{'*':^10}",f"{' ':^27}",f"{'*':^10}")
    print(f"{'***':^10}",f"{' ':^27}",f"{'***':^10}")
    print("********** Thanx from subnetcalculator **********")
    print("*************************************************")
    
def make_bitstream_from_address(numberlist):
    bitstream=""
    for item in numberlist:
            bitstream +=f"{item:08b}"
    return bitstream

def convert_list_to_numbers(lst):
    lst2=[]
    for el in lst:
        lst2.append(int(el,2))    
    return lst

def convert_list_to_bytes(lst):
    lst2=[]
    for el in lst:
        lst2.append(f"{el:08b}")       
    return lst2

#output is list of int
def ask_for_number_sequence(message):
    a=input(message)
    if a=='q':
        print("Program exited, thank you..")
        sys.exit(0)
    lst=str.split(a,sep=".")
    numberlist=[]
    for item in lst:
        numberlist.append(int(item))
    #make_bitstream_from_address(numberlist)
    return numberlist

#output is list int   
def is_valid_ip_address(numberlist):
    if len(numberlist)!=4:
        return False
    else:
        for number in numberlist:
            if number>=0 and number<255:
                continue
            else:
                return False
    return numberlist

def is_valid_netmask(numberlist):   
    if len(numberlist) !=4:
        return False
    else:
        binary_netmask=""
        for item in numberlist:
            binary_netmask +=f"{item:08b}"
    ##loop binary_netmask
    checking_ones=True
    counter=1
    for bit in binary_netmask:
        #print(counter)
        #counter +=1
        if bit=='1':
            #print(bit + "-->" + str(checking_ones))
            if checking_ones==True:
                continue
            else:
                return False
        else:
            checking_ones=False
            #print(bit + "-->" + str(checking_ones))
            continue
    return True
    
def one_bits_in_netmask(numberlist_subnet):
    binary_netmask=make_bitstream_from_address(numberlist_subnet)
    counter=0
    for char in binary_netmask:
        if char=='1':
            counter +=1
        else:
            break
    return counter



##2 lists of 4 bytes, perform bitwise AND on items[0] from each list,
##then on items[1], and so on, return 1 list in decimal notation

#output is list of int
def apply_subnet_mask(numberlist,numberlist_subnet):
    n=convert_list_to_bytes(numberlist)
    ns=convert_list_to_bytes(numberlist_subnet)
    subnet=[]
    i=0
    for item in n: 
        a=int(n[i],2)
        b=int(ns[i],2)
        c= a & b
        subnet.append(c)
        i +=1
    return subnet

#output is list of int
def netmask_to_wildcard_mask(numberlist_subnet):
    wildcard_mask=[]
    for el in numberlist_subnet:
        w=""
        for bit in f"{el:08b}":
            if bit=="0":
                w+= "1"
            else:
                w+= "0"
        i=int(w,2)     
        wildcard_mask.append(i)
    return wildcard_mask

def get_broadcast_address(network_address,wildcard_mask):
    n=convert_list_to_bytes(network_address)
    ns=convert_list_to_bytes(wildcard_mask)
    broadcast_address=[]
    i=0
    for el in n:
        a=int(n[i],2)
        b=int(ns[i],2)
        c= a | b
        broadcast_address.append(c)
        i+=1
    return broadcast_address

def prefix_length_to_max_hosts(n):
    l=32-n
    h=(2**l)-2
    return h
###           
#start program-test
###
##messages
msg_ask_ip="* Enter an IPv4 please: "
msg_ask_subnet="* Enter a valid subnetmask please: "
msg_wrong_input_try_again="* This address was no correct format, please try again...or type q(uit) to exit: "
msg_good_input="* This was a correct address, thank you.."
#
##ask and check ip
welcome()
numberlist=ask_for_number_sequence(msg_ask_ip)
while is_valid_ip_address(numberlist) is False:
    print(msg_wrong_input_try_again)
    numberlist=ask_for_number_sequence(msg_ask_ip)
print(msg_good_input)
##ask and check subnetmask
numberlist_subnet=ask_for_number_sequence(msg_ask_subnet)
while is_valid_netmask(numberlist_subnet) is False:
    print(msg_wrong_input_try_again)
    numberlist_subnet=ask_for_number_sequence(msg_ask_subnet)    
print(msg_good_input)
##print ip and netmask in bits
print(f"{'* The IPv4 binary is:':<30}" + make_bitstream_from_address(numberlist))
print(f"{'* The netmask binary results:':<30}" + make_bitstream_from_address(numberlist_subnet))
##print number of bits in netmask
n=one_bits_in_netmask(numberlist_subnet)
print(f"{'* Number of one bits in the netmask: ':<45}" + str(n))
##get and print netmask
s=apply_subnet_mask(numberlist,numberlist_subnet)
print(f"{'* The subnet of this network is: ':<45}", f"{s[0]}.{s[1]}.{s[2]}.{s[3]}")
##get and print wildcardmask
w=netmask_to_wildcard_mask(numberlist_subnet)
print(f"{'* The wildcardmask is: ':<45}", f"{w[0]}.{w[1]}.{w[2]}.{w[3]}")
##get and print broadcastmask
b=get_broadcast_address(s,w)
print(f"{'* The broadcast address is: ':<45}", f"{b[0]}.{b[1]}.{b[2]}.{b[3]}")
##get and print number of hosts
h=prefix_length_to_max_hosts(n)
print(f"{'* The number of hosts is: ':<45}", str(h))
ending()
