#ask input IPv4-address
def askIPv4():
    return input("Geef een IPv4 adress (xxx.xxx.xxx.xxx) aub..: ")
#ask input subnetmask
def askSubnet():
    return input("Geef een subnetmask aub...:")
#function to check input is valid network-address/netmask
def checkinput(i):
    control_a=[]
    j=str.split(i,sep=".")
    #print("Input was=====",j)
    if len(j)!=4:
        return False
    else:
        for el in j:
            k=int(el)
            #print(k);
            if k>=0 and k<=255:
                a=1
            else:
                a=0
            control_a.append(a)
            #print(control_a)
    for e in control_a:
        if e==0:
            return False
    return True
#function to convert address to integers
def res(a):
    r=[]
    p=str.split(a,sep=".")
    for el in p:
        i=int(el)
        j=format(i,'08b')
        r.append(j)
        
    for e in r:
        if e != r[-1]:
            print(e,end=".")
        else:
            print(e)
    print("\n")
    return r
#unpack list function
def unpack(s):
    return ' '.join(str(x) for x in s)
#function to calculate subnet - network - address
def getNetworkId(c,d):
    netId=0
    mask=[]
    for el in d:
        if el != '00000000': 
            mask.append(c[netId])
            netId +=1
            #print(type(el))
    ln=unpack(mask)
    print("Het netwerk id is: ",ln)
    
    
#funtion to start program
def start_program():
    print(f"{'*****************                           **************':^60}")
    print(f"{'*****************                           **************':^60}")
    print(f"{'*****************Welcome to subnetCalculator**************':^60}")
    print(f"{'*****************                           **************':^60}")
    print(f"{'*****************                           **************':^60}")
    
#function to end program
def end_program():
    print("\n\n")
    print(f"{' TTTTTT  HH HH    A   NN  NN XX     XX':^80}")
    print(f"{'   TT    HH HH  AA AA NN  NN  XX   XX':^80}")
    print(f"{'  TT    HH HH  AA AA NNN NN   XX XX':^80}")
    print(f"{' TT    HHHHH  AAAAA NNNNNN    XXX':^80}")
    print(f"{'  TT    HH HH  AA AA NN NNN   XX XX':^80}")
    print(f"{'   TT    HH HH  AA AA NN  NN  XX   XX':^80}")
    print(f"{'   TT    HH HH  AA AA NN  NN XX     XX':^80}")
    print("\n\n")
    print("Thank you, this program ends...")
    print("\n\n")
#
#
#START PROGRAM
#
#
stop=["q","Q"]
start_program()
a=askIPv4()
while checkinput(a) is False:
    x=input("This address was not correct, please (C)ontinue or Q(uit): ") 
    if x in stop:
        end_program()
        exit()
    a=askIPv4()
print("Valid input, thank you")
print("You gave address:")
c=res(a)
b=askSubnet()
while checkinput(b) is False:
    print("This subnet wasn't right..maybe another..? ")
    b=askSubnet()
print("Valid input, thank you")    
d=res(b)
getNetworkId(c,d)
#
#
#END PROGRAM
#
#
