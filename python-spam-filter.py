#import modules
import os,ntpath,re
#state the spamwords in this var
spamwords=["inheritance","lottery","viagra"]
#set function to return this input into a raw string to avoid C: from trowing an exception
def investigate(input):
    return(input)


#start program
#ask user for path to map to check
p=input("Which map you want to check for spam?")
p1=investigate(p)
#make list of all file in this directory
files=os.listdir(p1)
#loop list and check contents
for file in files:
    t=os.path.basename(file)
    with open(os.path.join(p1,file)) as f:
        for line in f:
            list=line.split(" ")
            for item in list:
                #take input of upper and lowercase in account
                item=item.lower()
                if item in spamwords:
                    sp=0
                    print(t, "is a SPAM-file, please be cautious...")
                    break
                else:
                    sp=1
    if sp==1:
        print(t, "is geen spam")

#end program

#############
#test this path::
#C:/Users/steve van gastel/Desktop/OSScripting/python/python-spam-filter.py
#test output::
#= RESTART: C:/Users/steve van gastel/Desktop/OSScripting/python/python-spam-filter.py
#Which map you want to check for spam?C:\Users\steve van gastel\Desktop\OSScripting\python\SpamFilterMap
#file1.txt is geen spam
#file2.txt is geen spam
#file3.txt is a SPAM-file, please be cautious...
#file4.txt is geen spam
#file5.txt is geen spam
#file6.txt is a SPAM-file, please be cautious...
#file7 is geen spam
#file8.txt is geen spam
#file9.txt is a SPAM-file, please be cautious...

############
