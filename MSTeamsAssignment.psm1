﻿function New-CustomChannel {
[CmdletBinding()]
param(
[Parameter(Mandatory,ValueFromPipelineByPropertyName)]
[string] $GroupId,
[Parameter(Mandatory,ValueFromPipelineByPropertyName)]
[string] $csvFile
) #end param
Begin{
} #end begin
Process{
$c = Get-Credential
Connect-MicrosoftTeams -Credential $c

#[PSCustomObject]@{Channels= Import-Csv -Path 'C:\Users\steve van gastel\Desktop\OSScripting\newchannels.csv' } | ForEach-Object {$my_name='Steve' ; [PSCustomObject]@{ChannelName= "$_ $my_name" ; GroupId=$GroupId}; write-host "$_.ChannelName"}

#C:\Users\steve van gastel\Desktop\OSScripting\newchannels.csv

Import-Csv -Path $csvFile | 
ForEach-Object {$my_name= 'Steve Van Gastel' ; $var = $_.ChannelName ; [PSCustomObject]@{ChannelName="$var $my_name"; GroupId=$GroupId}} | Write-Host


#ForEach-Object {$my_name= 'Steve Van Gastel' ; [PSCustomObject]@{ChannelName= "$_.ChannelName $my_name" ; GroupId=$GroupId} } | Get-Member

} #end process
End{
} #end end

} #end function